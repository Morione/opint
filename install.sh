#!/bin/bash



EXIST_PIP=('pip -V')
${EXIST_PIP}
if [ $? -eq 0 ]; then
    echo 'ok ... pip is installed'
else
    echo '!! ... pip is not installed'
    python -m ensurepip --default-pip
fi

EXIST_VIRTUALENV=('virtualenv --version')
${EXIST_VIRTUALENV}
if [ $? -eq 0 ]; then
    echo 'ok ... virtualenv is installed'
else
    echo '!! ... virtualenv is not installed'
    pip install virtualenv
fi

# stiahnutie aktualnych zdrojovych kodov
git clone git@bitbucket.org:Morione/opint.git

# presun do korenoveho adresara aplikacie
cd opint

# vytvorenie virtualneho prostredia pre python2
virtualenv env2 --python python2

# spustenie virtualneho prostredia
env/bin/activate

# instalacia kniznic
pip install -r requirements.txt

echo
echo 'OPINT script has been installed successfully'
echo