import requests


def merge_dict(source, destination):
	"""
	Merge input dictionary to output dictionary
	:param source: dictionary to merge
	:param destination: final output
	:return: final merged dictionary
	"""
	for k, v in source.items():
		if k in destination:
			if isinstance(v, list):
				for i in v:
					if i not in destination[k]:
						destination[k].append(i)
			else:
				if v not in destination[k]:
					destination[k].append(v)
		else:
			destination[k] = []
			if isinstance(v, list):
				for i in v:
					if i not in destination[k]:
						destination[k].append(i)
			else:
				destination[k] = [v]

	return destination


def flat_dict(connections):
	"""
	Get flat dictionary from dictionary with multiple array
	:param connections: input dictionary
	:return: flat dictionary
	"""
	child_domains = []
	for d in connections:
		child_domains.append(connections[d])
	return [item for sublist in child_domains for item in sublist]


def test_online_page(url):
	site = "http://" + url
	try:
		requests.get(site, verify=False)
		return True
	except Exception as e:
		return False


def clean_tracking_code(tracking_code):
	if tracking_code.count("-") > 1 and 'ca-pub' not in tracking_code:
		return tracking_code.rsplit("-", 1)[0]
	elif tracking_code.count("-") > 1:
		return tracking_code[3:]
	else:
		return tracking_code
