import requests
import json
from tools.logger import Output
from tools import cache


spyonweb_access_token = "DaV5oRLi1MOy"
spyonweb_url = "https://api.spyonweb.com/v1/"

o = Output(path="./logs/")
cache = cache.DbCache("cache.db")

#
# Send a request off to Spy On Web
#
def spyonweb_request(data, request_type="domain"):
	"""
	A Request to api.spyonweb.com
	The request https://api.spyonweb.com/v1/adsense/pub-1556223355139109?access_token=DjlnNO8Hbl3o
	and example of the response:
	{
		"status": "found",
		"result": {
			"adsense": {
			"pub-1556223355139109": {
				"fetched": 42,
				"found": 42,
							"items": {
								"bakertillysh.com": "2018-10-04",
								"brassbandtube.blogspot.com": "2012-04-23",
								"breathe07.blogspot.com": "2013-07-29",
								"chicago24.net": "2012-02-23",
								"cityoftime-roleplay.com": "2018-10-05",
								...
							}
				}
			}
		}
	}
	:param data: can be domain url [http://something.com], analytics code [UAXXXXXX] or
	adsence code [pub1234567890123456]
	:param request_type: can be 'domain', 'analytics' or 'adsense'
	:return json of result or None if it has found nothing
	"""

	params = {'access_token': spyonweb_access_token}

	url = spyonweb_url + request_type + "/" + data

	if not cache.is_cached(url):
		response = requests.get(url, params=params)
		cache.cache(url, json.dumps(response.json()))
		result = response.json()
	else:
		o.log("  ... cached url %s", url)
		result = json.loads(cache.get_cache(url))

	if 'status' in result:
		if result['status'] != "not_found" and result['status'] != "error":
			return result
	else:
		o.log("Request to SpyOnWeb API failed. Response : %s", result)

	return None


def spyonweb_analytics_codes(connections):
	"""
	Function to check the extracted analytics codes with Spyonweb API
	"""
	founded_connections = {}
	request_type = "domain"

	# use any found tracking codes on Spyonweb
	for code in connections:
		founded_connections[code] = []
		# send off the tracking code to Spyonweb
		if code.lower().startswith("pub"):
			request_type = "adsense"

		elif code.lower().startswith("ua"):
			request_type = "analytics"

		o.log("[*] Trying code: %s on Spyonweb.", code)

		results = spyonweb_request(code, request_type)

		if results and ('result' in results):
			for domain in results['result'][request_type][code]['items']:

				if domain not in connections[code]:
					o.log("[*] Found additional domain: %s", domain)
					founded_connections[code].append(domain)

	return founded_connections


#
# Use Spyonweb to grab full domain reports.
#
def spyonweb_domain_reports(connections):
	# now loop over all of the domains and request a domain report
	tested_domains = []
	all_codes = connections.keys()

	for code in all_codes:
		for domain in connections[code]:

			if domain not in tested_domains:
				tested_domains.append(domain)

				o.log("[*] Getting domain report for: %s", domain)

				results = spyonweb_request(domain)

				if results and ('result' in results):

					# loop over adsense results
					adsense = results['result'].get("adsense")

					if adsense:
						for code in adsense:

							code = clean_tracking_code(code)

							if code not in connections:
								connections[code] = []

							for domain in adsense[code]['items'].keys():
								if domain not in connections[code]:
									o.log("[*] Discovered new domain: %s", domain)
									connections[code].append(domain)

					analytics = results['result'].get("analytics")

					if analytics:
						for code in analytics:
							code = clean_tracking_code(code)

							if code not in connections:
								connections[code] = []

							for domain in analytics[code]['items'].keys():
								if domain not in connections[code]:
									o.log("[*] Discovered new domain: %s", domain)
									connections[code].append(domain)

	return connections