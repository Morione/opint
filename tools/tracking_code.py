import re
import requests
from tools.wayback import Wayback
from tools.logger import Output
from tools.helper import clean_tracking_code

google_adsense_pattern = re.compile("\"pub-[0-9]{1,}|\"ca-pub-[0-9]{1,}", re.IGNORECASE)
google_analytics_pattern = re.compile("ua-\d+-\d+", re.IGNORECASE)

o = Output(path="./logs/")
#
# Extract tracking codes from a target domain.
#
def extract_tracking_codes(base_domains, wayback_past_years=2):
	site_connections = {}

	w = Wayback()

	ext_domains = base_domains[:]

	# get history of domains (wayback machine api)
	if wayback_past_years > 0:
		for domain in ext_domains:
			if type(domain) is str:
				wayback_urls = w.closest(domain, years=wayback_past_years)
				base_domains.extend(_unique_list(wayback_urls))
				o.log("[*] Getting wayback url for %s .", domain)

	for domain in base_domains[:]:

		if domain is "" or (type(domain) is not str and type(domain) is not unicode):
			base_domains.remove(domain)
			continue

		# send a request off to the website
		try:

			o.log("[*] Checking %s for tracking codes.", domain)
			site = domain
			if domain.startswith("www"):
				domain = domain[4:]

			if not domain.startswith("http"):
				site = "http://" + domain

			headers = {
				'Accept-Encoding': 'gzip, deflate, sdch',
				'Accept-Language': 'en-US,en;q=0.8',
				'Upgrade-Insecure-Requests': '1',
				'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
				'Cache-Control': 'max-age=0',
				'Connection': 'keep-alive',
			}
			response = requests.get(site, verify=False, headers=headers, timeout=10)

		except requests.exceptions.RequestException as e:
			o.error("[!] Failed to reach site {}. \n{}", param=domain, error=e)
			continue

		# extract the tracking codes
		extracted_codes = []
		adsense_codes = google_adsense_pattern.findall(response.content)
		for k, val in enumerate(adsense_codes):
			adsense_codes[k] = val[1:]
		extracted_codes.extend(_unique_list(adsense_codes))

		analytics_codes = google_analytics_pattern.findall(response.content)
		extracted_codes.extend(_unique_list(analytics_codes))

		# loop over the extracted tracking codes
		for code in extracted_codes:

			# remove the trailing dash and number
			code = clean_tracking_code(code)

			# if code.lower() not in tracking_codes:

			o.log("[*] Discovered: %s", code.lower())

			if code not in site_connections.keys():
				site_connections[code] = [domain]
			else:
				site_connections[code].append(domain)

	return site_connections


def _unique_list(source):
	result = []
	for item in source:
		if item not in result:
			result.append(item)
	return result
