#!/usr/bin/env bash

source env2/bin/activate

if [ -z "$3" ]
then
      wayback=0
else
      wayback=$3
fi
python script.py --file $1 --graph $2 --wayback $wayback

deactivate