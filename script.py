import argparse

from tools.spyonweb import spyonweb_request, spyonweb_analytics_codes, spyonweb_domain_reports
from tools.tracking_code import extract_tracking_codes
from tools.logger import Output
from tools.output_builder import OutputBilder
from tools.helper import merge_dict, flat_dict, test_online_page, clean_tracking_code

o = Output(path="./logs/")
out = OutputBilder()
wayback_past_years = 0

# setup the commandline argument parsing
parser = argparse.ArgumentParser(description='Generate visualizations based on tracking codes.')

parser.add_argument("--graph",
					help="Output filename of the graph file. Example: bellingcat.gexf",
					default="connections.gexf")

parser.add_argument("--domain",
					help="A domain to try to find connections to.", )

parser.add_argument("--file",
					help="A file that contains domains, one per line.")

parser.add_argument("--wayback",
					help="How many years will the WayBack API crawl to past",
					default=0)

parser.add_argument("--apikey",
					help="SpyOnWeb API key")

args = parser.parse_args()

# build a domain list either by the file passed in
# or create a single item list with the domain passed in


o.log("\n[*] Start crawling ...")

if args.apikey:
	spyonweb_access_token = args.apikey

if args.wayback:
	wayback_past_years = int(args.wayback)

if args.file:
	with open(args.file, "rb") as fd:
		domains = fd.read().splitlines()
else:
	domains = [args.domain]

source_domains = domains[:]
source_codes = extract_tracking_codes(domains, wayback_past_years)

domains_from_source = spyonweb_analytics_codes(source_codes)

source_codes_2 = {}

for key, domains in domains_from_source.iteritems():
	for domain in domains:
		sc = extract_tracking_codes([domain])
		if sc:
			merge_dict(sc, source_codes_2)

final = {}
final = merge_dict(source_codes, final)
final = merge_dict(source_codes_2, final)

for dom in source_domains[:]:
	if not test_online_page(dom):
		source_domains.remove(dom)

# now create a graph of the final connections
out.graph_connections(final, source_domains, args.graph)

# summary of whole process will be in output csv file
out.summary(final, source_domains, args.graph)

o.log("[*] Finished! Open %s in Gephi!", args.graph)