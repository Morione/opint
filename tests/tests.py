import unittest

from tools import wayback
from tools import logger
from tools import whois
from tools import cache
from tools import output_builder
import os
import requests
import json


class ToolsTest(unittest.TestCase):

	def setUp(self):

		self.wb = wayback.Wayback()

	def test_closest(self):
		"""
		Test the normal operation of the closest method.

		"""

		url = "http://en.wikipedia.org/wiki/Alan_turing"
		archive_url = self.wb.closest(url)

		self.assertEqual(200, self.wb.status_code, self.wb.status_code)
		self.assertIsNotNone(
			self.wb.archive_timestamp, self.wb.archive_timestamp)
		self.assertNotEqual('', self.wb.archive_url, self.wb.archive_url)
		self.assertNotEqual('', archive_url, archive_url)

	def test_calculation_of_past_years(self):
		past_one_year_ts = self.wb._get_year("20181203110026", 1)
		self.assertEqual("20171203", past_one_year_ts[0:8])

		past_4_years_ts = self.wb._get_year("20181203110026", 4)
		self.assertEqual("20141203", past_4_years_ts[0:8])

	def test_past_3_years_archives(self):
		result = self.wb.closest("google.sk", "20181203110026", 3)
		self.assertEqual(3, len(result))

	def test_past_0_years_archives(self):
		result = self.wb.closest("google.sk", "20181203110026", 0)
		self.assertEqual(0, len(result))

	def test_past_3_years_but_2_exists(self):
		result = self.wb.closest("morione.sk", "20181203110026", 3)
		self.assertEqual(2, len(result))

	def test_past_2_years_but_none_exists(self):
		result = self.wb.closest("mkkmmkkm.sk", "20181203110026", 2)
		self.assertEqual(0, len(result))

	def test_logger(self):
		"""
		Test custom logger

		"""

		o = logger.Output()
		o.log("Some log without param")
		o.log("Some log with one param %s", "XYZ")
		o.log("Some log with one param %s", ["XYZ"])
		o.error("Some error log without param")
		o.error("Some error log with one param %s", "XYZ")
		try:
			raise Exception('This is the exception you expect to handle')
		except Exception as error:
			o.error("Some error log with one param {} and error \n{}", error, "XYZ")
		os.remove(o.logfile)

	def test_request_wayback_machine(self):
		response = requests.get("http://web.archive.org/web/20171206011708/https://www.alza.sk/")
		print(response)
		self.assertEqual(200, response.status_code)

	def test_whois_when_domain_exists(self):
		result = whois.Command.get_info("morione.sk")
		self.assertIsNotNone(result)
		self.assertNotEqual([], result)
		for r in result:
			if "Country Code" in r:
				self.assertEqual("SK", r["Country Code"])

	def test_whois_when_domain_doesnt_exist(self):
		result = whois.Command.get_info("xxmorione2.sk")
		self.assertIsNotNone(result)
		#FIXME: changed output
		#self.assertEqual("SK", result["domain"])
		#self.assertTrue("Country Code" not in result)

	def test_whois_who_is_com(self):
		result = whois.WhoIsCom.get_info("morione.sk")
		self.assertIsNotNone(result)
		#self.assertNotEqual([], result)
		#FIXME: changed output
		#for r in result:
		#	if "Country Code" in r:
		#		self.assertEqual("SK", r["Country Code"])

	def test_caching(self):
		db_name = "test.db"
		c = cache.DbCache(db_name)
		c.cache("tes2t", "hohohoho")
		self.assertTrue(c.is_cached("tes2t"))
		self.assertEqual("hohohoho", c.get_cache("tes2t"))
		os.remove(db_name)

	def test_caching_json(self):
		db_name = "test.db"
		c = cache.DbCache(db_name)
		data = {'jsonKey': None}
		c.cache("testjson", json.dumps(data))
		self.assertTrue(c.is_cached("testjson"))

		out = json.loads(c.get_cache("testjson"))
		self.assertEqual(data, out)

	# def test_parse_domain_file(self):
	#
	# 	w = whois.WhoIsCom()
	# 	oo = output_builder.OutputBilder()
	# 	out = w.parse_file("../data/domain2.txt")
	# 	with open("country.csv", "w") as f:
	# 		for k, v in enumerate(out):
	# 			f.writelines("{}, {}\n".format(v.rstrip(), oo._print_info(out[v])))
	# 			print("{}, {}".format(v.rstrip(), oo._print_info(out[v])))
	# 	f.close()
	#
	# def test_country_to_domain(self):
	# 	county = dict()
	#
	# 	with open("country_with_wayback.csv", "r") as f:
	# 		for k, v in enumerate(f):
	# 			w = v.rstrip().split(',')
	# 			county[w[0]] = w[1]
	# 	f.close()
	#
	# 	final = []
	# 	with open("final_domain_with_wayback.csv", "r") as f:
	# 		for k, v in enumerate(f):
	# 			ww = v.rstrip().split(';')
	# 			ww[3] = county[ww[1]]
	# 			final.append(ww)
	# 	f.close()
	#
	# 	with open("final2_domain_with_wayback.csv", "w") as f:
	#
	# 		for item in final:
	# 			f.writelines("{}, {}, {}, {}\n".format(item[0], item[1], item[2], item[3]))
	#
	# 	f.close()

